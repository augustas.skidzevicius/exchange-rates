'use strict';

function getRates(url) {
  return fetch(url)
    .then(resp => resp.json())
    .catch(err => console.log(`Fething latest: ${err}`));
}

function getDate(today, subtract) {
  const date = new Date(today);
  const year = date.getFullYear() - subtract;
  const month = (date.getMonth() + 1) > 9 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1);
  const day = date.getDate();
  return `${year}-${month}-${day}`;
}

function getMaxMinRate(data, type) {
  return type === 'min'
    ? Object.keys(data).reduce((a, b) => (data[a] < data[b]) ? a : b)
    : Object.keys(data).reduce((a, b) => (data[a] > data[b]) ? a : b);
}

function loadRates() {
  getRates('https://api.ratesapi.io/api/latest').then(data => {
    const today = data.date;
    const lowestRate = getMaxMinRate(data.rates, 'min');
    const highetRate = getMaxMinRate(data.rates, 'max');
    const fiveYearsAgo = getRates(`https://api.ratesapi.io/api/${getDate(today, 5)}?symbols=${lowestRate},${highetRate}`);
    const tenYearAgo = getRates(`https://api.ratesapi.io/api/${getDate(today, 10)}?symbols=${lowestRate},${highetRate}`);
    
    console.log(`${data.date}: ${data.base} rates for \n${lowestRate} - ${data.rates[lowestRate]} \n${highetRate} - ${data.rates[highetRate]}`);
    return Promise.all([fiveYearsAgo, tenYearAgo])
  }).then(data => {
    data.forEach(rate => {
      for (let [key, value] of Object.entries(rate.rates)) {
        console.log(`${rate.date}: ${key} - ${value}`);
      }
    });
  });
}

loadRates();
